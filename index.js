const express = require('express')
const bunyan = require('bunyan')
const {LoggingBunyan} = require('@google-cloud/logging-bunyan')
const cloudLogger = new LoggingBunyan()
// logs com in levels
// trace
// debug
// info
// warn
// error
// fatal
const app = express()
const config = {
    name:'signup-app',
    streams: [
        {stream:process.stdout, level:'info'},
        {path:'signups.log', type:'file'},
        cloudLogger.stream('info')
    ]

}

const logger = bunyan.createLogger(config)

app.get('/signup/:email', (req,res)=>{
    const email = req.params.email
    if (email.includes("@")){
    logger.info('A new person registered with email ' + email)
    res.send('your email ' + email + ' is being processed')
    } else {
        logger.warn('A person tried to us an invalid email ' + email)
        res.send('That is an inappropriate email')
    }

})

app.listen(3000,()=>console.log('app started'))